# Food Delivery Service

## Description
This service has the following functionality:
1. Registration and authorization system.
2. Division of roles into the following entities:
   `User`, `Restaurant`, `Courier`, `Admin`.
3. Ordering food from the restaurant - pickup or delivery.
4. In case of delivery, the courier will 
bring the order to the destination address.

## Technologies used
`Java`, `PostgreSQL`, `Spring Boot`, `Spring Data JPA`,
`Spring Security`.