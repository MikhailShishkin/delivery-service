package com.b00mshaka1aka.delivery.entity;

import jakarta.persistence.*;

import java.util.UUID;

@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    UUID id;

    @Column(name = "phone_number")
    String phoneNumber;
    String password;
}
